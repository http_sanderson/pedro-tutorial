package main

import (

	"net/http"
	"time"
	"log"
	"fmt"

)

type myHandler struct {}

func (my myHandler) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	username := req.URL.Query().Get("username")
	
	time , err := pessoasTable[username].gettime()
	
	if (err != nil) {
	
		res.Write([]byte("Alguma coisa deu errada"))
		return
	}
	
	fmt.Fprintf(res, "Time: %s" , time)
	
	//
	
	
	
	

}

func StartServer() {

	s := &http.Server{
	
		Addr:           "172.22.51.135:8080",
		Handler:        myHandler{},
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		
	}
	
	log.Fatal(s.ListenAndServe())


}
